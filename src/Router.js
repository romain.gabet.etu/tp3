export default class Router {
	static titleElement;
	static contentElement;
	static routes = [];
	static #menuElement;
	static set menuElement(element) {
	this.#menuElement = element;
	// au clic sur n'importe quel lien contenu dans "element"
	// déclenchez un appel à Router.navigate(path)
	// où "path" est la valeur de l'attribut `href=".."` du lien cliqué
		let links = this.#menuElement.querySelectorAll("a");
		links.forEach(element => {
			element.addEventListener('click',event =>{
				event.preventDefault();
				let href = event.currentTarget.getAttribute("href");
				this.navigate(href);
			});
		});
	}

	static navigate(path) {
		
		const route = this.routes.find(route => route.path === path);
		if (route) {
			this.titleElement.innerHTML = `<h1>${route.title}</h1>`;
			this.contentElement.innerHTML = route.page.render();
			route.page.mount?.(this.contentElement);

			document.querySelectorAll('a').forEach(element => {
				element.classList.remove("active");
			});
			document.querySelector('a[href="'+path+'"]').classList.add("active");
			window.history.pushState(null, null , path);
		}
	}
}
