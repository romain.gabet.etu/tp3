import Page from './Page';

export default class PizzaForm extends Page {
	render() {
		return /*html*/ `
			<form class="pizzaForm">
				<label>
					Nom :
					<input type="text" name="name">
				</label>
				<button type="submit">Ajouter</button>
			</form>`;
	}

	mount(element) {
		super.mount(element);

        const form = element.querySelector('form');
        form.addEventListener('submit', this.submit);
	}

	submit(event) {
        event.preventDefault();
        const input = document.querySelector('input[name=name]');
        if(!input.value){
            alert();
        }else{
            console.log("La pizza "+input.value+" a été ajoutée");
            input.value="";
        }
       
    }
}
